'use strict';

var _, async, express, multipart, cache, validator, fs;

// third-party-dependencies
_ = require('lodash');
async = require('async');
express = require('express');
multipart = require('connect-multiparty');
cache = require('lru-cache');
validator = require('validator');

// node.js dependencies
fs = require('fs');


function init_movie_router(app) {
  var movies, slug_regex, movie_genres_regex, movee_util, config, BadInput, NotFound, movie, movie_id_validator;

  movies = express.Router();
  slug_regex = app.get('slug_regex');
  movie_genres_regex = app.get('movie_genres_regex');
  movee_util = app.get('movee_util');
  config = app.get('config');
  BadInput = app.get('BadInput');
  NotFound = app.get('NotFound');
  movie = app.get('movie_resource');

  movie_id_validator = movee_util.validate({
    movie_id: ['isInt', slug_regex]
  });

  function resolveMovieSlug(req, res, next) {
    var slug = req.safe_params.movie_id;

    if (!validator.isInt(slug) && validator.matches(slug, slug_regex)) {
      movie.where({
        slug: slug
      }).fetch({
        columns: ['id']
      }).then(function(movie) {
        req.safe_params.movie_id = ( !! movie) ? movie.id : -1;
        next();
      }, next);
    } else {
      return next();
    }
  }

  movies.route('/')

  .get(function movie_query(req, res, next) {

    movie
      .fetchAll({
        columns: ['id', 'slug', 'title', 'description', 'year', 'genre', 'created_at', 'updated_at']
      })
      .then(function(all_movies) {
        res.json(200, all_movies);
      }, next);

  })

  .post(multipart(), function movie_create(req, res, next) {
    var cover, movie_title, movie_description, movie_year, movie_genre, movie_filename;

    if (!req.body.movie_title)
      return next(new BadInput('missing-title'));

    if (!req.body.movie_description)
      return next(new BadInput('missing-description'));

    movie_title = req.body.movie_title;
    movie_description = req.body.movie_description;

    if (!req.files.cover) {
      return next(new BadInput('missing-cover'));
    }

    if (req.files.cover.type != 'image/jpeg') {
      return next(new BadInput('invalid-cover-type'));
    }

    if (!/^[\d]{4}$/.test(req.body.movie_year)) {
      return next(new BadInput('invalid-movie-year'));
    } else {
      movie_year = req.body.movie_year;
    }

    if (!movie_genres_regex.test(req.body.movie_genre)) {
      return next(new BadInput('invalid-movie-genre'));
    } else {
      movie_genre = req.body.movie_genre;
    }

    if (!fs.existsSync(config.movies_directory + req.body.movie_filename)) {
      return next(new BadInput('movie-filename-not-found'));
    } else {
      movie_filename = req.body.movie_filename;
    }

    async.waterfall([

      function(callback) {
        fs.readFile(req.files.cover.path, callback);
      },
      function(cover_buffer, callback) {
        cover = cover_buffer;
        async.retry(3, function(cb) {
          fs.unlink(req.files.cover.path, cb);
        }, function(err) {
          if (err)
            return callback(err);

          callback(null);
        });
      },
      function(callback) {
        var movie_data = {};
        movie_data.title = movie_title;
        movie_data.description = movie_description;
        movie_data.filename = movie_filename;
        movie_data.cover = cover;
        movie_data.year = movie_year;
        movie_data.genre = movie_genre;

        movie.forge(movie_data).save().exec(callback);
      }
    ], function(err, new_movie) {
      if (err)
        return next(err);

      res.json(201, new_movie.omit('cover', 'filename'))
    });
  }, function(err, req, res, next) {
    // if an error occured, throw away the uploaded cover (asyncronously of course)
    next(err);
    fs.unlink(req.files.cover.path);
  });

  movies.route('/:movie_id')

  .all(movie_id_validator, resolveMovieSlug)

  .get(function movie_get(req, res, next) {
    movie.where({
      id: req.safe_params.movie_id
    }).fetch({
      columns: ['id', 'slug', 'title', 'description', 'year', 'genre', 'created_at', 'updated_at']
    }).then(function(movie) {
      if (movie)
        res.json(200, movie);
      else
        return next(new NotFound());
    }, next);
  });

  movies.get('/:movie_id/cover', movie_id_validator, resolveMovieSlug, function movie_get_cover(req, res, next) {
    movie.where({
      id: req.safe_params.movie_id
    }).fetch({
      columns: ['cover']
    }).then(function(movie) {
      if (movie) {
        res.set('content-type', 'image/jpeg');
        res.send(200, movie.get('cover'));
      } else
        return next(new NotFound());
    }, next);
  })

  return movies;
}

module.exports = init_movie_router;