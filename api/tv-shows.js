'use strict';

var _, async, express, multipart, cache, validator, fs;

// third-party-dependencies
_ = require('lodash');
async = require('async');
express = require('express');
multipart = require('connect-multiparty');
cache = require('lru-cache');
validator = require('validator');

// node.js dependencies
fs = require('fs');

function init_tv_shows_router(app) {
  var tv_shows, slug_regex, tv_genres_regex, movee_util, config, BadInput, NotFound, show, season, episode, tv_shows_id_validator;

  tv_shows = express.Router();
  slug_regex = app.get('slug_regex');
  tv_genres_regex = app.get('tv_genres_regex');
  movee_util = app.get('movee_util');
  config = app.get('config');
  BadInput = app.get('BadInput');
  NotFound = app.get('NotFound');
  show = app.get('show_resource');
  season = app.get('season_resource');
  episode = app.get('episode_resource');

  tv_shows_id_validator = movee_util.validate({
    show_id: ['isInt', slug_regex]
  });

  function resolveTvShowSlug(req, res, next) {
    var slug = req.safe_params.show_id || req.param('show_id');

    if (!validator.isInt(slug) && validator.matches(slug, app.get('slug_regex'))) {

      show.where({
        slug: slug
      }).fetch({
        columns: ['id']
      }).then(function(show) {
        req.safe_params.show_id = ( !! show) ? show.id : -1;
        next();
      }, next);

    } else {
      return next();
    }
  }

  tv_shows.route('/')

  .get(function tv_shows_query(req, res, next) {

    show.fetchAll({
      columns: ['id', 'slug', 'title', 'description', 'year', 'genre', 'created_at', 'updated_at']
    }).then(function(shows) {
      res.json(200, shows);
    }, next);

  })

  tv_shows.route('/:show_id')

  .all(tv_shows_id_validator, resolveTvShowSlug)

  .get(function tv_show_get(req, res, next) {
    show.where({
      id: req.safe_params.show_id
    }).fetch({
      columns: ['id', 'slug', 'title', 'description', 'year', 'genre', 'created_at', 'updated_at'],
      withRelated: ['seasons']
    }).then(function(show) {
      if (show)
        res.json(200, show);
      else
        return next(new NotFound());
    }, next);
  });

  tv_shows.get('/:show_id/cover', tv_shows_id_validator, resolveTvShowSlug, function tv_show_get_cover(req, res, next) {
    show.where({
      id: req.safe_params.show_id
    }).fetch({
      columns: ['cover']
    }).then(function(show) {
      if (show) {
        res.set('content-type', 'image/jpeg');
        res.send(200, show.get('cover'));
      } else
        return next(new NotFound);
    }, next);
  });

  tv_shows.route('/:show_id/seasons')

  .all(tv_shows_id_validator, resolveTvShowSlug)

  .get(function tv_show_get_seasons(req, res, next) {
    season.where(req.safe_params).fetchAll().then(function(seasons) {
      res.json(200, seasons);
    }, next);
  });

  tv_shows.route('/:show_id/seasons/:season_number')

  .all(movee_util.validate({
    show_id: ['isInt', slug_regex],
    season_number: 'isInt'
  }), resolveTvShowSlug)

  .get(function tv_show_get_season(req, res, next) {
    season.where(req.safe_params).fetch({
      withRelated: ['episodes']
    }).then(function(season) {
      if (season)
        res.json(200, season);
      else
        return next(new NotFound);
    }, next);
  })

  tv_shows.route('/:show_id/seasons/:season_number/episodes')

  .all(movee_util.validate({
    show_id: ['isInt', slug_regex],
    season_number: 'isInt'
  }), resolveTvShowSlug)

  .get(function tv_show_get_episodes(req, res, next) {
    episode.where(req.safe_params).fetchAll({
      columns: ['id', 'show_id', 'season_id', 'title', 'synopsis', 'date_aired', 'created_at', 'updated_at', 'episode_number', 'season_number']
    }).then(function(episodes) {
      res.json(200, episodes);
    }, next);
  })

  tv_shows.route('/:show_id/seasons/:season_number/episodes/:episode_number')

  .all(movee_util.validate({
    show_id: ['isInt', slug_regex],
    season_number: 'isInt',
    episode_number: 'isInt'
  }), resolveTvShowSlug)

  .get(function tv_show_get_episode(req, res, next) {
    episode.where(req.safe_params).fetch().then(function(episode) {
      if (episode)
        res.json(200, episode.omit('filename'));
      else
        return next(new NotFound);
    }, next);
  })

  return tv_shows;
}

module.exports = init_tv_shows_router;