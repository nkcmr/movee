'use strict';
var express, http, https, logger, compress, app, sqlite3, movie, movies, show, season, episode, async, fs, api, bodyParser, multipart, genres, movie_genres_regex, tv_genres_regex, cache, cover_cache, passport, BasicStrategy, cookieParser, expressSession, config, uuid, send, _, errors, BadInput, NotFound, knex, bookshelf, movee_util, validator, tv_show_api, movie_api;

// third-party dependencies
sqlite3 = require('sqlite3');
async = require('async');

knex = require('knex')({
  client: 'sqlite',
  connection: {
    filename: __dirname + '/database.sqlite',
    debug: true
  },
});
bookshelf = require('bookshelf')(knex);

express = require('express');
logger = require('morgan');
compress = require('compression');
bodyParser = require('body-parser');
cookieParser = require('cookie-parser');
expressSession = require('express-session');
multipart = require('connect-multiparty');
cache = require('lru-cache');
passport = require('passport');
BasicStrategy = require('passport-http').BasicStrategy;
uuid = require('node-uuid');
send = require('send');
_ = require('lodash');
validator = require('validator');

// node.js dependencies
fs = require('fs');
http = require('http');
https = require('https');

// internal dependencies
movee_util = require(__dirname + '/movee-util.js');
config = require(__dirname + '/config.js');
errors = require(__dirname + '/errors.js');
BadInput = errors.BadInput;
NotFound = errors.NotFound;

// database stuff

movie = bookshelf.Model.extend({
  tableName: 'movies',
  hasTimestamps: true
});

show = bookshelf.Model.extend({
  tableName: 'shows',
  hasTimestamps: true,
  seasons: function() {
    return this.hasMany(season);
  }
});

season = bookshelf.Model.extend({
  tableName: 'seasons',
  hasTimestamps: true,
  show: function() {
    return this.belongsTo(show);
  },
  episodes: function() {
    return this.hasMany(episode);
  }
});

episode = bookshelf.Model.extend({
  tableName: 'episodes',
  hasTimestamps: true,
  show: function() {
    return this.belongsTo(show);
  },
  season: function() {
    return this.belongsTo(season);
  }
});


genres = ['action-adventure', 'kids-family', 'comedy', 'classics', 'documentary', 'drama', 'foreign', 'horror', 'independent', 'music', 'romance', 'sci-fi-fantasy', 'short-film', 'sports', 'thriller', 'western'];
movie_genres_regex = /action-adventure|kids-family|comedy|classics|documentary|drama|foreign|horror|independent|music|romance|sci-fi-fantasy|short-film|sports|thriller|western/g;

tv_genres_regex = /animation|classics|comedy|drama|kids-family|non-fiction|reality-tv|sci-fi-fantasy|sports/;


// security stuff

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use(new BasicStrategy(function(username, password, done) {
  if (username == config.primary_user.username) {
    if (password == config.primary_user.password) {
      return done(null, {
        username: username
      });
    } else {
      return done(null, false);
    }
  } else {
    return done(null, false);
  }
}));

// express stuff

app = express();

app.use(function(req, res, next) {
  if (config.server.https_enabled) {
    if (req.secure)
      return next()
    else
      return res.redirect('https://' + config.server.hostname + ':' + config.server.https_port + '/');
  }

  next();
})

app.set('slug_regex', (/^[a-z0-9|-]+$/));
app.set('movie_genres_regex', movie_genres_regex);
app.set('tv_genres_regex', tv_genres_regex);
app.set('movee_util', movee_util);
app.set('config', config);
app.set('BadInput', BadInput);
app.set('NotFound', NotFound);
app.set('movie_resource', movie);
app.set('show_resource', show);
app.set('season_resource', season);
app.set('episode_resource', episode);

// mount to /api/tv-shows
tv_show_api = require(__dirname + '/api/tv-shows')(app);
// mount to /api/movies
movie_api = require(__dirname + '/api/movies')(app);

app.use(compress());
app.use(cookieParser());
app.use(bodyParser());
app.use(expressSession({
  secret: config.cookie_secret
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(passport.authenticate('basic'));

app.use(express.static(__dirname + '/public'));
app.use(logger('short'));

// api stuff

api = express.Router();

api.get('/server-info', function(req, res, next) {
  res.json(200, {
    hostname: config.server.hostname,
    directories: {
      movies: config.movies_directory,
      tv: config.tv_shows_directory
    }
  })
});

/*

                                         o8o                     
                                         `"'                     
ooo. .oo.  .oo.    .ooooo.  oooo    ooo oooo   .ooooo.   .oooo.o 
`888P"Y88bP"Y88b  d88' `88b  `88.  .8'  `888  d88' `88b d88(  "8 
 888   888   888  888   888   `88..8'    888  888ooo888 `"Y88b.  
 888   888   888  888   888    `888'     888  888    .o o.  )88b 
o888o o888o o888o `Y8bod8P'     `8'     o888o `Y8bod8P' 8""888P' 

*/

api.use('/movies', movie_api);

/*

    .                                oooo                                            
  .o8                                `888                                            
.o888oo oooo    ooo          .oooo.o  888 .oo.    .ooooo.  oooo oooo    ooo  .oooo.o 
  888    `88.  .8'          d88(  "8  888P"Y88b  d88' `88b  `88. `88.  .8'  d88(  "8 
  888     `88..8'   8888888 `"Y88b.   888   888  888   888   `88..]88..8'   `"Y88b.  
  888 .    `888'            o.  )88b  888   888  888   888    `888'`888'    o.  )88b 
  "888"     `8'             8""888P' o888o o888o `Y8bod8P'     `8'  `8'     8""888P' 

*/

api.use('/tv-shows', tv_show_api);

// api.route('/tv-shows')
//   .get(function tv_show_index(req, res, next) {
//     show.fetchAll({
//       columns: ['id', 'title', 'description', 'year', 'genre', 'created_at', 'updated_at']
//     }).then(function(shows) {
//       res.json(200, shows);
//     }, next);
//   });

// var tv_show_validator = movee_util.validate({
//   show_id: [
//     app.get('slug_regex'),
//     'isInt'
//   ]
// });

// api.route('/tv-shows/:show_id')
//   .get(tv_show_validator, resolveTvShowSlug, function tv_show_get(req, res, next) {
//     show.where({
//       id: req.safe_params.show_id
//     }).fetch({
//       columns: ['id', 'slug', 'title', 'description', 'year', 'genre', 'created_at', 'updated_at'],
//       withRelated: ['seasons']
//     }).then(function(show) {
//       if ( !! show) {
//         return res.json(200, show);
//       } else {
//         return next(new NotFound());
//       }
//     }, next);
//   });
// api.get('/tv-shows/:show_id/cover', tv_show_validator, resolveTvShowSlug, function tv_show_cover(req, res, next) {
//   show.where({
//     id: req.safe_params.show_id
//   }).fetch({
//     columns: ['cover']
//   }).then(function(show) {
//     if ( !! show) {
//       res.set('content-type', 'image/jpeg');
//       res.send(200, show.get('cover'));
//     } else {
//       return next(new NotFound());
//     }
//   }, next);
// });
/*

 .oooo.o  .ooooo.   .oooo.    .oooo.o  .ooooo.  ooo. .oo.    .oooo.o 
d88(  "8 d88' `88b `P  )88b  d88(  "8 d88' `88b `888P"Y88b  d88(  "8 
`"Y88b.  888ooo888  .oP"888  `"Y88b.  888   888  888   888  `"Y88b.  
o.  )88b 888    .o d8(  888  o.  )88b 888   888  888   888  o.  )88b 
8""888P' `Y8bod8P' `Y888""8o 8""888P' `Y8bod8P' o888o o888o 8""888P' 

*/

// api.route('/tv-shows/:show_id/seasons')
//   .get(tv_show_validator, resolveTvShowSlug, function seasons_index(req, res, next) {
//     season.where(req.safe_params).fetchAll().then(function(seasons) {
//       res.json(200, seasons);
//     }, next);
//   });

// api.route('/tv-shows/:show_id/seasons/:season_number')
//   .get(movee_util.validate({
//     show_id: ['isInt', app.get('slug_regex')],
//     season_number: 'isInt'
//   }), resolveTvShowSlug, function season_get(req, res, next) {
//     season.where(req.safe_params).fetch({
//       withRelated: ['episodes']
//     }).then(function(season) {
//       if (season) {
//         res.json(200, season);
//       } else {
//         return next(new NotFound());
//       }
//     }, next);
//   })

/*

                      o8o                           .o8                     
                      `"'                          "888                     
 .ooooo.  oo.ooooo.  oooo   .oooo.o  .ooooo.   .oooo888   .ooooo.   .oooo.o 
d88' `88b  888' `88b `888  d88(  "8 d88' `88b d88' `888  d88' `88b d88(  "8 
888ooo888  888   888  888  `"Y88b.  888   888 888   888  888ooo888 `"Y88b.  
888    .o  888   888  888  o.  )88b 888   888 888   888  888    .o o.  )88b 
`Y8bod8P'  888bod8P' o888o 8""888P' `Y8bod8P' `Y8bod88P" `Y8bod8P' 8""888P' 
           888                                                              
          o888o                                                             
                                                                            
*/

// api.post('/episodes', function() {});
// api.get('/episodes/:episode_id', movee_util.validate({
//   episode_id: 'isInt'
// }), function episode_get(req, res, next) {
//   episode.where({
//     id: req.safe_params.episode_id
//   }).fetch().then(function(episode) {
//     if (episode)
//       res.json(200, episode.omit('filename'));
//     else
//       return next(new NotFound());
//   }, next);
// })

api.use(movee_util.catch_error('BadInput', function(err, req, res) {
  res.json(400, {
    message: err.message,
    error_code: err.error_code
  });
}));

api.use(movee_util.catch_error('NotFound', function(err, req, res) {
  res.json(404, {
    message: err.message
  });
}));

// END api stuff

app.use('/api', api);

app.get('/stream/movie/:movie_id', movee_util.validate({
  movie_id: 'isInt'
}), function movie_stream(req, res, next) {
  movie.where({
    id: req.safe_params.movie_id
  }).fetch({
    columns: ['filename']
  }).then(function(movie) {
    if (movie) {
      if (fs.existsSync(config.movies_directory + movie.get('filename'))) {
        send(req, movie.get('filename'), {
          root: config.movies_directory,
          etag: false
        }).pipe(res);
      } else {
        return next(new Error('movie-file-does-not-exist'));
      }
    } else {
      return next(new NotFound());
    }
  }, next);
});

app.get('/stream/tv-show/:show_id/season/:season_id/episode/:episode_id', function tv_show_stream(req, res, next) {

});

app.use(function(req, res, next) {
  res.set('content-type', 'text/plain');
  res.send(404, 'all i found was some dust... probably wasn\'t what you were looking for, sorry. (404 Not Found)');
});

app.use(movee_util.catch_error('NotFound', function(err, req, res) {
  res.set('content-type', 'text/plain');
  res.send(404, 'all i found was some dust... probably wasn\'t what you were looking for, sorry. (404 Not Found)');
}));

app.use(function(err, req, res, next) {
  console.error(err.stack || err);
  res.json(500, {
    message: 'server-error'
  });
});

http.Server(app).listen(config.server.http_port);

if (config.server.https_enabled) {
  config.server.ssl_ca_chain.map(function(val, idx, chain) {
    return fs.readFileSync(val);
  });

  https.Server({
    key: fs.readFileSync(config.server.ssl_key),
    cert: fs.readFileSync(config.server.ssl_cert),
    ca: config.server.ssl_ca_chain
  }, app).listen(config.server.https_port);
}