'use strict';
var _, async, validator, util, errors, BadInput;

_ = require('lodash');
_.str = require('underscore.string');
async = require('async');
validator = require('validator');
util = require('util');

errors = require(__dirname + '/errors.js');
BadInput = errors.BadInput;

function validate(params) {
  if (!params || params.constructor.name != 'Object')
    throw new Error('params must be an Object');

  function runCheck(value_to_check, checker, level) {
    level = level || 0;
    if (level > 1)
      return false;

    if (checker.constructor.name == 'RegExp') {
      return validator.matches(value_to_check, checker);
    } else if (checker.constructor.name == 'String' && validator.hasOwnProperty(checker)) {
      return validator[checker](value_to_check);
    } else if (checker.constructor.name == 'Array' && checker.length) {
      for (var i = 0; i < checker.length; i++) {
        if (runCheck(value_to_check, checker[i], level + 1))
          return true;
      }

      return false;
    } else {
      throw new Error(util.format('validator does not have a \'%s\' function', checker));
    }
  }

  return function(req, res, next) {
    req.safe_params = {};

    for (var prop in params) {
      var check_val = req.param(prop);
      var checker = params[prop];

      if (runCheck(check_val, checker))
        req.safe_params[prop] = check_val;
      else
        return next(new BadInput(util.format('invalid-%s', _.str.slugify(prop))));
    }

    next();
  }

}

exports.validate = validate;

function catch_error(error_s, handler) {
  return function(err, req, res, next) {
    if (_.isArray(error_s)) {
      if (_.contains(error_s, err.name)) {
        handler(err, req, res);
      } else {
        return next(err);
      }
    } else if (_.isString(error_s)) {
      if (error_s == err.name) {
        handler(err, req, res);
      } else {
        return next(err);
      }
    } else {
      throw new Error('catch_error requires a string or an array for parameter 1');
    }
  }
}

exports.catch_error = catch_error;