(function(global) {

  'use strict';

  var show_genres = [{
    label: 'animation',
    value: 'animation'
  }, {
    label: 'classics',
    value: 'classics'
  }, {
    label: 'comedy',
    value: 'comedy'
  }, {
    label: 'drama',
    value: 'drama'
  }, {
    label: 'kids & family',
    value: 'kids-family'
  }, {
    label: 'non fiction',
    value: 'non-fiction'
  }, {
    label: 'reality tv',
    value: 'reality-tv'
  }, {
    label: 'sci-fi & fantasy',
    value: 'sci-fi-fantasy'
  }, {
    label: 'sports',
    value: 'sports'
  }];

  var movie_genres = [{
    label: 'action & adventure',
    value: 'action-adventure'
  }, {
    label: 'kids & family',
    value: 'kids-family'
  }, {
    label: 'comedy',
    value: 'comedy'
  }, {
    label: 'classics',
    value: 'classics'
  }, {
    label: 'documentary',
    value: 'documentary'
  }, {
    label: 'drama',
    value: 'drama'
  }, {
    label: 'foreign',
    value: 'foreign'
  }, {
    label: 'horror',
    value: 'horror'
  }, {
    label: 'independent',
    value: 'independent'
  }, {
    label: 'music',
    value: 'music'
  }, {
    label: 'romance',
    value: 'romance'
  }, {
    label: 'sci-fi & fantasy',
    value: 'sci-fi-fantasy'
  }, {
    label: 'short film',
    value: 'short-film'
  }, {
    label: 'sports',
    value: 'sports'
  }, {
    label: 'thriller',
    value: 'thriller'
  }, {
    label: 'western',
    value: 'western'
  }];

  var util = angular.module('movee.util', [])

  util.value('$dropzone', global.Dropzone);
  util.constant('movie_genres', movie_genres);
  util.constant('show_genres', show_genres);

})(this);