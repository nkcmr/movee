(function(global) {

  'use strict';
  var movee = angular.module('movee.controllers', []);

  movee.controller('TvShowsCtrl', ['$log', '$scope', 'shows', '$routeParams',
    function($log, $scope, shows, $routeParams) {
      $scope.view = ( !! $routeParams.view && /grid|list/.test($routeParams.view)) ? $routeParams.view : 'list';
      $scope.media_endpoint = 'tv-shows';
      $scope.media_type = 'show';
      $scope.media = shows;
    }
  ]);

  movee.controller('MovieIndexCtrl', ['$log', '$scope', 'movies', '$routeParams',
    function($log, $scope, movies, $routeParams) {
      $scope.view = ( !! $routeParams.view && /grid|list/.test($routeParams.view)) ? $routeParams.view : 'list';
      $scope.media_endpoint = 'movies';
      $scope.media_type = 'movie';
      $scope.media = movies;
    }
  ]);

  movee.controller('MovieAddCtrl', ['$log', '$scope', '$dropzone', '$location', '$timeout',
    function($log, $scope, $dropzone, $location, $timeout) {
      var movieAddDropZone;
      $log.debug('[MovieAddCtrl] - hi.');

      $scope.in_progress = false;
      $scope.error_message = '';
      $scope.message = '';


      $dropzone.options.movieAdd = {
        paramName: 'cover',
        maxFilesize: 1,
        maxFiles: 1,
        autoProcessQueue: false,
        clickable: true,
        maxThumbnailFilesize: 5,
        thumbnailWidth: 350,
        thumbnailHeight: Number.MAX_SAFE_INTEGER,
        previewTemplate: '<div class="dz-preview"><div class="dz-thumbnail-container"><img data-dz-thumbnail></div></div>',
        dictDefaultMessage: 'drop the movie cover here'
      };

      $dropzone.discover();
      movieAddDropZone = $dropzone.forElement('#movie-add');

      var msg_timer = null;

      $scope.addMovie = function() {
        if (movieAddDropZone.files.length == 0) {
          $scope.message = 'please add a cover for the movie. (good source of movie covers: http://themoviedb.org)';
          $timeout(function() {
            $scope.message = '';
          }, 5000);
          return;
        }

        $scope.in_progress = true;

        movieAddDropZone.once('success', function(cover, response, xhr) {
          $scope.message = 'successfully added movie!';
          $timeout(function() {
            $location.url('/movie/' + response.id);
          }, 2000);
        });

        movieAddDropZone.once('error', function(cover, response, xhr) {
          console.log(response);

          var messages = {};
          messages['server-error'] = 'something unknown happened while trying to add the movie';
          messages['missing-cover'] = 'please add a cover to the movie';
          messages['missing-title'] = 'all movies have a title, if it doesn\'t, just make one up i guess';
          messages['missing-description'] = 'you forgot to add a description for the movie';
          messages['invalid-cover-type'] = 'please use only jpegs for covers';
          messages['invalid-movie-year'] = 'something ain\'t right about that movie year';
          messages['invalid-movie-genre'] = 'invalid genre';
          messages['movie-filename-not-found'] = 'we looked for that movie file but couldn\'t find it :/';

          switch (response.error_code || response.message) {
            case 'invalid-cover-type':
              movieAddDropZone.removeAllFiles();
              break;
            case 'invalid-movie-year':
              $scope.movie_year = '';
              break;
            case 'invalid-movie-genre':
              $scope.movie_genre = '';
              break;
            case 'movie-filename-not-found':
              $scope.movie_filename = '';
              break;
          }

          if ((response.error_code || response.message) != 'invalid-cover-type')
            movieAddDropZone.files[0].status = 'queued';

          $scope.$apply(function() {
            $scope.in_progress = false;
            $scope.error_message = messages[response.error_code || response.message] || 'an unknown error has occured';
          });
          $timeout.cancel(msg_timer);
          msg_timer = $timeout(function() {
            $scope.error_message = '';
          }, 5000);
        });

        movieAddDropZone.processQueue();
      };


      $scope.$watch('cover', function(newVal) {
        $log.debug(newVal);
      });
    }
  ]);

  movee.controller('PlayCtrl', ['$log', '$scope', '$routeParams', 'media',
    function($log, $scope, $routeParams, media) {
      $scope.media = media;
      $scope.media_type = $routeParams.media_type;
    }
  ]);

})(this);