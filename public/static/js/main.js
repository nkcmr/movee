(function(global) {
  'use strict';

  var movee = angular.module('movee', ['ngRoute', 'ngResource', 'movee.controllers', 'movee.util']);

  movee.run(['$location', '$rootScope', 'movie_genres', 'show_genres', '$http',
    function($location, $root, movie_genres, show_genres, $http) {

      $root.isLocationActive = function(location) {
        return (location == $location.path());
      }

      $root.getWindowOrigin = function() {
        return global.location.origin;
      }

      $root.movie_genres = movie_genres;
      $root.show_genres = show_genres;

      $http.get('/api/server-info').then(function(response) {
        $root.server_info = response.data;
      });
    }
  ]);

  movee.config(['$compileProvider',
    function($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/http|https|vlc/);
    }
  ]);

  movee.config(['$routeProvider',
    function($routeProvider) {

      $routeProvider.when('/play/movie/:movie_id', {
        templateUrl: '/static/views/play-movie.html',
        controller: 'PlayMovieCtrl',
        resolve: {
          movie: ['MovieResource', '$route',
            function(MovieResource, $route) {
              return MovieResource.get($route.current.params).$promise;
            }
          ]
        }
      });

      $routeProvider.when('/play/tv-show/:show_slug/episode/:episode_id', {
        templateUrl: '/static/views/play-tv-show.html',
        controller: 'PlayTvShowCtrl',
        resolve: {
          episode: ['EpisodeResource', '$route',
            function(EpisodeResource, $route) {
              return EpisodeResource.get($route.current.params).$promise;
            }
          ]
        }
      })

      $routeProvider.when('/play/:media_type/:media_id', {
        templateUrl: '/static/views/play.html',
        controller: 'PlayCtrl',
        resolve: {
          media: ['$http', '$log', '$q', '$route',
            function($http, $log, $q, $route) {
              $log.debug('getting media');
              var media_type, media_id, deferred;

              deferred = $q.defer();

              media_type = $route.current.params.media_type;
              media_id = $route.current.params.media_id;

              $http.get('/api/' + media_type + '/' + media_id).then(function(response) {

                deferred.resolve(response.data);

              }, deferred.reject);

              return deferred.promise;
            }
          ]
        }
      })

      $routeProvider.when('/movies', {
        templateUrl: '/static/views/media-index.html',
        controller: 'MovieIndexCtrl',
        resolve: {
          movies: ['MovieResource', '$route',
            function(MovieResource, $route) {
              return MovieResource.query($route.current.params).$promise;
            }
          ]
        }
      });

      $routeProvider.when('/movies/add', {
        templateUrl: '/static/views/movies-add.html',
        controller: 'MovieAddCtrl'
      });

      $routeProvider.when('/tv-shows', {
        templateUrl: '/static/views/media-index.html',
        controller: 'TvShowsCtrl',
        resolve: {
          shows: ['ShowResource', '$route',
            function(ShowResource, $route) {
              return ShowResource.query($route.current.params).$promise;
            }
          ]
        }
      });

      $routeProvider.otherwise({
        redirectTo: '/movies'
      });

    }
  ]);

  movee.filter('genre', ['movie_genres', 'show_genres',
    function(movie_genres, show_genres) {
      return function(input) {
        for (var i = 0; i < movie_genres.length; i++) {
          if (input == movie_genres[i].value)
            return movie_genres[i].label;
        }

        for (var i = 0; i < show_genres.length; i++) {
          if (input == show_genres[i].value)
            return show_genres[i].label;
        }

        return 'other';
      }
    }
  ]);

  movee.service('MovieResource', ['$resource',
    function($resource) {
      return $resource('/api/movies/:movie_id', {
        movie_id: '@id'
      });
    }
  ]);

  movee.service('ShowResource', ['$resource',
    function($resource) {
      return $resource('/api/tv-shows/:show_id', {
        show_id: '@id'
      });
    }
  ]);

  movee.service('SeasonResource', ['$resource',
    function($resource) {
      return $resource('/api/tv-shows/:show_id/seasons/:season_number');
    }
  ]);

  movee.service('EpisodeResource', ['$resource',
    function($resource) {
      return $resource('/api/episodes/:episode_id', {
        episode_id: '@id'
      });
    }
  ]);

})(this);