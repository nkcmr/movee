var config;

config = {};
config.server = {};

// what will your servers global hostname be?
config.server.hostname = 'my-example-server.com';

// what will unsecure http port be? [remember to forward this port!]
config.server.http_port = 80;

config.server.https_enabled = false; // true or false

// what will the secure https port be? [again, forward this port when behind a firewall!]
config.server.https_port = 443;

config.server.ssl_key = '/absolute/path/to/ssl/key';
config.server.ssl_cert = '/absolute/path/to/ssl/cert';
config.server.ssl_ca_chain = ['/absolute/path/to/ssl/ca-chain-cert1', '/absolute/path/to/ssl/ca-chain-cert2'];

// primary user login info
config.primary_user = {};
config.primary_user.username = 'admin';
config.primary_user.password = 'namaste';

// cookie session secret - just put something long and random here
config.cookie_secret = 'change_me';

// media directories

// where are all of your movies?  !!(with a '/' on the end)
config.movies_directory = '/where/are/your/movies/';

// where are all of your tv shows? !!(with a '/' on the end)
config.tv_shows_directory = '/where/are/your/tv-shows/';

// dont edit below this line! -------------------------------------------------
module.exports = config;