function BadInput(error_code) {
  if (!error_code)
  // throw new Error('BadInput must have an \'error_code\'!');

    Error.apply(this, Array.prototype.slice.call(arguments));
  this.name = 'BadInput';
  this.message = 'invalid-input';
  this.error_code = error_code || 'unknown-bad-input';
}
BadInput.prototype = Error.prototype;
BadInput.prototype.constructor = BadInput;

exports.BadInput = BadInput;

function NotFound(message) {
  Error.apply(this, Array.prototype.slice.call(arguments));
  this.name = 'NotFound';
  this.message = 'not-found';
}
NotFound.prototype = Error.prototype;
NotFound.prototype.constructor = NotFound;

exports.NotFound = NotFound;